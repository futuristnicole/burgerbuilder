import React, { Component } from 'react';
import './App.css';
import Layout from './components/Layout/Layout';
import BurgerBuilder from './containers/BurgerBuilder/BurgerBuilder';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Layout>
          <h1>I Love Yash</h1>  
          <BurgerBuilder />        
        </Layout>
      </div>
    );
  }
}

export default App;
